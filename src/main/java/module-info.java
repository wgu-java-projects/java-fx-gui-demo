module com.example.guidemo {
    requires javafx.controls;
    requires javafx.graphics;
   // requires javafx.swing;
    requires javafx.fxml;
    requires java.desktop;


    opens com.example.guidemo to javafx.fxml;
    exports com.example.guidemo;
}