package com.example.guidemo;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

public class GUIDemoController implements Initializable {
    // These items were for the Checkbox example
    @FXML
    private Label pizzaOrderLabel;
    @FXML
    private CheckBox pepperoniCheckBox;
    @FXML
    private CheckBox pineappleCheckBox;
    @FXML
    private CheckBox baconCheckBox;

    // These items are for the ChoiceBox example
    @FXML
    private ChoiceBox choiceBox;
    @FXML
    private Label choiceboxLabel;

    // These itens are for the ComboBox example
    @FXML
    private ComboBox comboBox;
    @FXML
    private Label comboBoxLabel;

    // These items are for the RadioButton example
    @FXML
    private RadioButton phpRadioButton;
    @FXML
    private RadioButton javaRadioButton;
    @FXML
    private RadioButton cSharpRadioButton;
    @FXML
    private RadioButton cPlusPlusRadioButton;
    @FXML
    private Label radioButtonLabel;
    @FXML
    private  ToggleGroup favLangToggleGroup;

    // These items are for the ListView and TextArea example
    @FXML
    private ListView listView;
    @FXML
    private TextArea golfTextArea;

    // This is the spinner object to store grade information
    @FXML private Spinner gradeSpinner;
    @FXML private Button getGradesButton;
    @FXML private Label gradeLabel;

    /**
     * When this method is called, it will change the Scene to
     * a TableView example
     */
    public void changeScreenButtonPushed(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("ExampleOfTableView.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
        window.setScene(tableViewScene);
        window.show();
    }


    /**
     * This will update the Label of Choice Box
     */
    @FXML
    public void choiceBoxButtonPushed() {
        choiceboxLabel.setText("My favorite fruit is: \n" + choiceBox.getValue().toString());
    }

    /**
     * This will update the Label of Checkbox Box
     */
    @FXML
    public void pizzaOrderButtonPushed() {
        String order = "Toppings are:";

        if (pepperoniCheckBox.isSelected())
            order += "\npepperoni";
        if (pineappleCheckBox.isSelected())
            order += "\npineapple";
        if (baconCheckBox.isSelected())
            order += "\nbacon";
        this.pizzaOrderLabel.setText(order);
    }

    /**
     * This will update the comboBoxLabel when the ComboBox is changed
     */
    public void comboBoxWasUpdated() {
        this.comboBoxLabel.setText("Course selected: \n" + comboBox.getValue().toString());
    }

    /**
     * This method will update the radioButtonLabel when ever a different
     * radio button is pushed
     **/

    @FXML
    public void radioButtonChanged() {
        if (this.favLangToggleGroup.getSelectedToggle().equals(this.phpRadioButton))
            radioButtonLabel.setText("The selected item is: PHP");
        if (this.favLangToggleGroup.getSelectedToggle().equals(this.javaRadioButton))
            radioButtonLabel.setText("The selected item is: Java");
        if (this.favLangToggleGroup.getSelectedToggle().equals(this.cSharpRadioButton))
            radioButtonLabel.setText("The selected item is: C#");
        if (this.favLangToggleGroup.getSelectedToggle().equals(this.cPlusPlusRadioButton))
            radioButtonLabel.setText("The selected item is: C++");
    }


    /**
     * This method will copy the Strings from the ListView and put them in the text area
     */
    public void listViewButtonPushed(){
        String textAreaString = "";

        ObservableList listOfItems = listView.getSelectionModel().getSelectedItems();

        for (Object item : listOfItems) {
            textAreaString += String.format("%s%n", (String) item);
        }
        this.golfTextArea.setText(textAreaString);
    }

    /**
     * This method will read from the grade Spinner and update the label when pushed
     */
    public void getGetGradesButtonPushed() {
        this.gradeLabel.setVisible(true);
        this.gradeLabel.setText(this.gradeSpinner.getValue().toString());
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        pizzaOrderLabel.setText("");

        // This items are for configuring the ChoiceBox example
        choiceboxLabel.setText("");
        choiceBox.getItems().add("apples");
        choiceBox.getItems().add("bananas");
        choiceBox.getItems().addAll("oranges", "pears", "strawberries");
        choiceBox.setValue("apples");

        // This items are for configuring the ComboBox
        comboBoxLabel.setText("");
        comboBox.getItems().add("COMP1030");
        comboBox.getItems().addAll("COMP1008", "MGMT2003", "MGMT2010");
        comboBox.setValue("COMP1030");

        // These items are for configuring the Radio Button
        radioButtonLabel.setText("");
        favLangToggleGroup = new ToggleGroup();
        this.phpRadioButton.setToggleGroup(favLangToggleGroup);
        this.javaRadioButton.setToggleGroup(favLangToggleGroup);
        this.cSharpRadioButton.setToggleGroup(favLangToggleGroup);
        this.cPlusPlusRadioButton.setToggleGroup(favLangToggleGroup);

        // These items are for configuring the ListArea
        listView.getItems().addAll("Golf Balls", "Wedges", "Irons", "Tees", "Driver", "Putter");
        listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        // Configure the Spinner with values of 0-100
        SpinnerValueFactory<Integer> gradesValueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(0,100,75);
        this.gradeSpinner.setValueFactory(gradesValueFactory);
        gradeSpinner.setEditable(true);
        this.gradeLabel.setVisible(false);
    }
}